package io.gitlab.teammycelium.devotion.client.gui;

import io.gitlab.teammycelium.devotion.config.ClientGlobalConfig;
import io.gitlab.teammycelium.devotion.config.SharedGlobalConfig;
import io.gitlab.teammycelium.devotion.util.Globals;
import io.gitlab.teammycelium.devotion.util.I19n;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.DummyConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;

import java.util.ArrayList;
import java.util.List;

public class GuiEiraIRCConfig extends GuiConfig {

    public GuiEiraIRCConfig(GuiScreen parentScreen) {
        super(parentScreen, getCategories(), Globals.MOD_ID, "global", false, false, I19n.format("eirairc:gui.config"));
    }

    private static List<IConfigElement> getCategories() {
        List<IConfigElement> list = new ArrayList<>();
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.general.shared"), "devotion:config.category.general", getCombinedConfigElements(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.GENERAL), ClientGlobalConfig.thisConfig.getCategory(ClientGlobalConfig.GENERAL))));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.screenshots"), "devotion:config.category.screenshots", new ConfigElement(ClientGlobalConfig.thisConfig.getCategory(ClientGlobalConfig.SCREENSHOTS)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.notifications"), "devotion:config.category.notifications", new ConfigElement(ClientGlobalConfig.thisConfig.getCategory(ClientGlobalConfig.NOTIFICATIONS)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.theme"), "devotion:config.category.theme", new ConfigElement(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.THEME)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.bot"), "devotion:config.category.bot", new ConfigElement(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.BOT)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.settings"), "devotion:config.category.settings", new ConfigElement(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.SETTINGS)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.compatibility"), "devotion:config.category.compatibility", new ConfigElement(ClientGlobalConfig.thisConfig.getCategory(ClientGlobalConfig.COMPATIBILITY)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.network"), "devotion:config.category.network", new ConfigElement(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.NETWORK)).getChildElements()));
        list.add(new DummyConfigElement.DummyCategoryElement(I18n.format("devotion:config.category.addons"), "devotion:config.category.addons", getCombinedConfigElements(SharedGlobalConfig.thisConfig.getCategory(SharedGlobalConfig.ADDONS), ClientGlobalConfig.thisConfig.getCategory(ClientGlobalConfig.ADDONS))));
        return list;
    }

    public static List<IConfigElement> getCombinedConfigElements(ConfigCategory... categories) {
        List<IConfigElement> list = new ArrayList<>();
        for (ConfigCategory category : categories) {
            list.addAll(new ConfigElement(category).getChildElements());
        }
        return list;
    }

    public static List<IConfigElement> getAllConfigElements(Configuration config) {
        List<IConfigElement> list = new ArrayList<>();
        for (String category : config.getCategoryNames()) {
            list.addAll(new ConfigElement(config.getCategory(category)).getChildElements());
        }
        return list;
    }
}
