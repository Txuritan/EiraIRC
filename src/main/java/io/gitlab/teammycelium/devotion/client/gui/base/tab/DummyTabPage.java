package io.gitlab.teammycelium.devotion.client.gui.base.tab;

public abstract class DummyTabPage extends GuiTabPage {

    public DummyTabPage(GuiTabContainer tabContainer, String title) {
        super(tabContainer, title);
    }

    @Override
    public abstract void tabClicked();

}
