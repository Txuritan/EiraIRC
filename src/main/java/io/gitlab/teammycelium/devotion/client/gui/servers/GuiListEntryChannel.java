package io.gitlab.teammycelium.devotion.client.gui.servers;

import io.gitlab.teammycelium.devotion.ConnectionManager;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.client.graphics.TextureRegion;
import io.gitlab.teammycelium.devotion.client.gui.EiraGui;
import io.gitlab.teammycelium.devotion.client.gui.base.list.GuiListTextEntry;
import io.gitlab.teammycelium.devotion.config.ChannelConfig;
import io.gitlab.teammycelium.devotion.util.Globals;
import net.minecraft.client.gui.FontRenderer;

public class GuiListEntryChannel extends GuiListTextEntry {

    private final GuiServerConfig parent;
    private final ChannelConfig config;
    private TextureRegion icon;

    public GuiListEntryChannel(GuiServerConfig parent, FontRenderer fontRenderer, ChannelConfig config, int height) {
        super(fontRenderer, config.getName(), height, Globals.TEXT_COLOR);
        this.parent = parent;
        this.config = config;

        IRCConnection connection = ConnectionManager.getConnection(parent.getServerConfig().getIdentifier());
        setJoined(connection != null && connection.getChannel(config.getName()) != null);
    }

    public ChannelConfig getConfig() {
        return config;
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        if (selected) {
            parent.channelSelected(config);
        }
    }

    @Override
    public void onDoubleClick() {
        parent.channelClicked(config);
    }

    public void setJoined(boolean isJoined) {
        if (isJoined) {
            icon = EiraGui.atlas.findRegion("icon_active");
        } else {
            icon = null;
        }
    }

    @Override
    public void drawEntry(int x, int y) {
        super.drawEntry(x + 11, y);

        if (icon != null) {
            icon.draw(x + 3, y + height / 2 - 6);
        }
    }
}
