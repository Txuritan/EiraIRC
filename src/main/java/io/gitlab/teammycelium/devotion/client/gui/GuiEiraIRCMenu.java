// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.client.gui;

import io.gitlab.teammycelium.devotion.client.gui.base.GuiMenuButton;
import io.gitlab.teammycelium.devotion.client.gui.screenshot.GuiScreenshots;
import io.gitlab.teammycelium.devotion.client.gui.servers.GuiServerConfigContainer;
import io.gitlab.teammycelium.devotion.util.I19n;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

public class GuiEiraIRCMenu extends EiraGuiScreen {

    private static final ResourceLocation meow = new ResourceLocation("mob.cat.meow");

    private static final int BUTTON_SIZE = 64;

    private GuiMenuButton btnServers;
    private GuiMenuButton btnTwitch;
    private GuiMenuButton btnHelp;
    private GuiMenuButton btnFriends;
    private GuiMenuButton btnScreenshots;
    private GuiMenuButton btnSettings;

    @Override
    public void initGui() {
        super.initGui();

        final int buttonCenterX = width / 2;
        final int buttonCenterY = height / 2;

        btnServers = new GuiMenuButton(0, I19n.format("devotion:gui.menu.servers"), buttonCenterX - 132, buttonCenterY - 95, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_servers"));
        buttonList.add(btnServers);

        btnTwitch = new GuiMenuButton(1, I19n.format("devotion:gui.menu.twitch"), buttonCenterX - 32, buttonCenterY - 95, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_twitch"));
        buttonList.add(btnTwitch);

        btnScreenshots = new GuiMenuButton(2, I19n.format("devotion:gui.menu.screenshots"), buttonCenterX + 64, buttonCenterY - 95, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_screenshots"));
        buttonList.add(btnScreenshots);

        btnFriends = new GuiMenuButton(3, I19n.format("devotion:gui.menu.channels"), buttonCenterX - 132, buttonCenterY, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_friends"));
        buttonList.add(btnFriends);

        btnHelp = new GuiMenuButton(4, I19n.format("devotion:gui.menu.help"), buttonCenterX - 32, buttonCenterY, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_cat"));
        btnHelp.setPlayButtonSound(false);
        buttonList.add(btnHelp);

        btnSettings = new GuiMenuButton(5, I19n.format("devotion:gui.menu.settings"), buttonCenterX + 64, buttonCenterY, BUTTON_SIZE, BUTTON_SIZE, EiraGui.atlas.findRegion("menu_settings"));
        buttonList.add(btnSettings);
    }

    @Override
    public void actionPerformed(GuiButton button) {
        if (button == btnTwitch) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiTwitch(this));
        } else if (button == btnServers) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiServerConfigContainer(this));
        } else if (button == btnScreenshots) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiScreenshots(this));
        } else if (button == btnSettings) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiEiraIRCConfig(this));
        } else if (button == btnHelp) {
            Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.ENTITY_CAT_PURREOW, 1f));
            Utils.openWebpage("http://blay09.net/?page_id=63");
        } else if (button == btnFriends) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiWelcome(this));
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        drawLightBackground(menuX, menuY, menuWidth, menuHeight);

        super.drawScreen(mouseX, mouseY, par3);
    }
}
