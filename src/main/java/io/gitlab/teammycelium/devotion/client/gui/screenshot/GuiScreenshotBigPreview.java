package io.gitlab.teammycelium.devotion.client.gui.screenshot;

import io.gitlab.teammycelium.devotion.client.gui.EiraGuiScreen;
import io.gitlab.teammycelium.devotion.client.gui.base.image.GuiFileImage;
import io.gitlab.teammycelium.devotion.client.gui.base.image.GuiImage;
import io.gitlab.teammycelium.devotion.client.gui.base.image.GuiURLImage;
import io.gitlab.teammycelium.devotion.client.screenshot.Screenshot;
import io.gitlab.teammycelium.devotion.util.Globals;
import io.gitlab.teammycelium.devotion.util.I19n;
import net.minecraft.client.gui.GuiScreen;

import java.net.URL;

public class GuiScreenshotBigPreview extends EiraGuiScreen {

    private final GuiImage image;

    public GuiScreenshotBigPreview(GuiScreen parentScreen, URL url) {
        super(parentScreen);
        image = new GuiURLImage(url);
        image.loadTexture();
    }

    public GuiScreenshotBigPreview(GuiScreen parentScreen, Screenshot screenshot) {
        super(parentScreen);
        image = new GuiFileImage(screenshot.getFile());
        image.loadTexture();
    }

    @Override
    public boolean mouseClick(int mouseX, int mouseY, int mouseButton) {
        gotoPrevious();
        return true;
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        image.dispose();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float p_73863_3_) {
        super.drawScreen(mouseX, mouseY, p_73863_3_);

        image.draw(0, 0, width, height, zLevel);

        String s = I19n.format("devotion:gui.image.clickToGoBack");
        drawString(fontRenderer, s, width - fontRenderer.getStringWidth(s) - 5, height - fontRenderer.FONT_HEIGHT - 5, Globals.TEXT_COLOR);
    }
}
