package io.gitlab.teammycelium.devotion.client.screenshot;

import io.gitlab.teammycelium.devotion.api.upload.UploadHoster;
import io.gitlab.teammycelium.devotion.api.upload.UploadedFile;
import io.gitlab.teammycelium.devotion.config.ClientGlobalConfig;
import io.gitlab.teammycelium.devotion.config.ScreenshotAction;

public class AsyncUploadScreenshot implements Runnable {

    private final UploadHoster hoster;
    private final Screenshot screenshot;
    private final ScreenshotAction followUpAction;
    private UploadedFile uploadedFile;
    private boolean complete;

    public AsyncUploadScreenshot(UploadHoster hoster, Screenshot screenshot, ScreenshotAction followUpAction) {
        this.hoster = hoster;
        this.screenshot = screenshot;
        this.followUpAction = followUpAction;
        Thread thread = new Thread(this, "ScreenshotUpload");
        thread.start();
    }

    @Override
    public void run() {
        uploadedFile = hoster.uploadFile(screenshot.getFile(), ClientGlobalConfig.uploadBufferSize.get());
        if (uploadedFile != null) {
            screenshot.setUploadedFile(uploadedFile);
        }
        complete = true;
    }

    public boolean isComplete() {
        return complete;
    }

    public ScreenshotAction getFollowUpAction() {
        return followUpAction;
    }

    public Screenshot getScreenshot() {
        return screenshot;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }
}
