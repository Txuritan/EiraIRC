// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.irc;

import com.google.common.collect.Maps;
import io.gitlab.teammycelium.devotion.api.config.IConfigManager;
import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCUser;
import io.gitlab.teammycelium.devotion.util.ConfigHelper;

import java.util.Collection;
import java.util.Map;

public class IRCChannelImpl implements IRCChannel {

    private IRCConnectionImpl connection;
    private String name;
    private String topic;
    private Map<String, IRCUser> users = Maps.newHashMap();

    public IRCChannelImpl(IRCConnectionImpl connection, String name) {
        this.connection = connection;
        this.name = name;
    }

    public Collection<IRCUser> getUserList() {
        return users.values();
    }

    public IRCUser getUser(String nick) {
        return users.get(nick.toLowerCase());
    }

    public void addUser(IRCUserImpl user) {
        users.put(user.getName().toLowerCase(), user);
    }

    public void removeUser(IRCUser user) {
        users.remove(user.getName().toLowerCase());
    }

    @Override
    public void message(String message) {
        connection.message(name, message);
    }

    @Override
    public void notice(String message) {
        connection.notice(name, message);
    }

    public String getName() {
        return name;
    }

    @Override
    public ContextType getContextType() {
        return ContextType.IRCChannel;
    }

    @Override
    public boolean hasTopic() {
        return topic != null;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    public IRCConnectionImpl getConnection() {
        return connection;
    }

    public boolean hasUser(String nick) {
        return users.containsKey(nick.toLowerCase());
    }

    public String getIdentifier() {
        return connection.getIdentifier() + "/" + name.toLowerCase();
    }

    @Override
    public void ctcpMessage(String message) {
        message(IRCConnectionImpl.CTCP_START + message + IRCConnectionImpl.CTCP_END);
    }

    @Override
    public void ctcpNotice(String message) {
        notice(IRCConnectionImpl.CTCP_START + message + IRCConnectionImpl.CTCP_END);
    }

    @Override
    public IConfigManager getGeneralSettings() {
        return ConfigHelper.getGeneralSettings(this).manager;
    }

    @Override
    public IConfigManager getBotSettings() {
        return ConfigHelper.getBotSettings(this).manager;
    }

    @Override
    public IConfigManager getThemeSettings() {
        return ConfigHelper.getTheme(this).manager;
    }
}
