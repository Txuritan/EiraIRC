// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.bot;

import io.gitlab.teammycelium.devotion.api.bot.IBotCommand;
import io.gitlab.teammycelium.devotion.api.bot.IRCBot;
import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCUser;
import io.gitlab.teammycelium.devotion.config.settings.BotSettings;
import io.gitlab.teammycelium.devotion.net.NetworkHandler;
import io.gitlab.teammycelium.devotion.net.message.MessageNotification;
import io.gitlab.teammycelium.devotion.util.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.apache.commons.lang3.StringUtils;

public class BotCommandMessage implements IBotCommand {

    @Override
    public String getCommandName() {
        return "msg";
    }

    @Override
    public boolean isChannelCommand() {
        return false;
    }

    @Override
    public void processCommand(IRCBot bot, IRCChannel channel, IRCUser user, String[] args, IBotCommand commandSettings) {
        BotSettings botSettings = ConfigHelper.getBotSettings(channel);
        if (!botSettings.allowPrivateMessages.get()) {
            user.notice(I19n.format("eirairc:commands.msg.disabled"));
        }
        String playerName = args[0];
        EntityPlayer entityPlayer = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(playerName);
        if (entityPlayer == null) {
            PlayerList playerEntityList = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList();
            for (EntityPlayer entity : playerEntityList.getPlayers()) {
                if (Utils.getNickGame(entity).equals(playerName) || Utils.getNickIRC(entity, channel).equals(playerName)) {
                    entityPlayer = entity;
                }
            }
            if (entityPlayer == null) {
                user.notice(I19n.format("eirairc:general.noSuchPlayer"));
                return;
            }
        }
        String message = StringUtils.join(args, " ", 1, args.length);
        if (botSettings.filterLinks.get()) {
            message = MessageFormat.filterLinks(message);
        }
        ITextComponent chatComponent = MessageFormat.formatChatComponent(botSettings.getMessageFormat().mcPrivateMessage, bot.getConnection(), null, user, message, MessageFormat.Target.Minecraft, MessageFormat.Mode.Message);
        String notifyMsg = chatComponent.getUnformattedText();
        if (notifyMsg.length() > 42) {
            notifyMsg = notifyMsg.substring(0, 42) + "...";
        }
        NetworkHandler.instance.sendTo(new MessageNotification(NotificationType.PrivateMessage, notifyMsg), ((EntityPlayerMP) entityPlayer));
        entityPlayer.sendMessage(chatComponent);
        user.notice(I19n.format("eirairc:bot.msgSent", playerName, message));
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }

    @Override
    public boolean broadcastsResult() {
        return false;
    }

    @Override
    public boolean allowArgs() {
        return true;
    }

    @Override
    public String getCommandDescription() {
        return "Send a private message to an online player.";
    }

}
