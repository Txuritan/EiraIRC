// Copyright (c) 2015, Christopher "BlayTheNinth" Baker


package io.gitlab.teammycelium.devotion.bot;

import io.gitlab.teammycelium.devotion.api.bot.IBotCommand;
import io.gitlab.teammycelium.devotion.api.bot.IRCBot;
import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCUser;
import io.gitlab.teammycelium.devotion.util.Utils;

public class BotCommandWho implements IBotCommand {

    public BotCommandWho() {
    }

    @Override
    public String getCommandName() {
        return "who";
    }

    @Override
    public boolean isChannelCommand() {
        return true;
    }

    @Override
    public void processCommand(IRCBot bot, IRCChannel channel, IRCUser user, String[] args, IBotCommand commandSettings) {
        if (commandSettings.broadcastsResult()) {
            Utils.sendPlayerList(channel);
        }
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }

    @Override
    public boolean broadcastsResult() {
        return true;
    }

    @Override
    public boolean allowArgs() {
        return false;
    }

    @Override
    public String getCommandDescription() {
        return "Prints out a list of all players online.";
    }

}
