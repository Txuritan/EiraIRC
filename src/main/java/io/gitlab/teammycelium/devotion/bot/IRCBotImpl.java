// Copyright (c) 2015, Christopher "BlayTheNinth" Baker


package io.gitlab.teammycelium.devotion.bot;

import io.gitlab.teammycelium.devotion.api.bot.IBotCommand;
import io.gitlab.teammycelium.devotion.api.bot.IRCBot;
import io.gitlab.teammycelium.devotion.api.event.ReloadBotCommandsEvent;
import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCUser;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.irc.IRCConnectionImpl;
import io.gitlab.teammycelium.devotion.irc.IRCUserImpl;
import io.gitlab.teammycelium.devotion.util.ConfigHelper;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraftforge.common.MinecraftForge;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class IRCBotImpl implements IRCBot {

    private final IRCConnectionImpl connection;
    private final Map<String, IBotCommand> commands = new HashMap<>();

    public IRCBotImpl(IRCConnectionImpl connection) {
        this.connection = connection;

        reloadCommands();
    }

    public void reloadCommands() {
        commands.clear();
        if (isServerSide()) {
            registerCommand(new BotCommandHelp());
            registerCommand(new BotCommandMessage());
            registerCommand(new BotCommandWho());
            registerCommand(new BotCommandOp());
            for (IBotCommand command : ConfigurationHandler.getCustomCommands()) {
                registerCommand(command);
            }
        }
        MinecraftForge.EVENT_BUS.post(new ReloadBotCommandsEvent(this));
    }

    @Override
    public void registerCommand(IBotCommand command) {
        commands.put(command.getCommandName().toLowerCase(), command);
    }

    public Collection<IBotCommand> getCommands() {
        return commands.values();
    }

    @Override
    public IRCConnection getConnection() {
        return connection;
    }

    public boolean processCommand(IRCChannel channel, IRCUser sender, String message) {
        String[] args = message.split(" ");
        if (ConfigHelper.getBotSettings(channel).disabledNativeCommands.get().containsString(args[0].toLowerCase(), true)) {
            return false;
        }
        IBotCommand botCommand = commands.get(args[0].toLowerCase());
        if (botCommand == null || (channel != null && !botCommand.isChannelCommand())) {
            return false;
        }
        String[] shiftedArgs = ArrayUtils.subarray(args, 1, args.length);
        if (botCommand.requiresAuth()) {
            ((IRCUserImpl) sender).queueAuthCommand(this, channel, botCommand, shiftedArgs);
        } else {
            botCommand.processCommand(this, channel, sender, shiftedArgs, botCommand);
        }
        return true;
    }

    @Override
    public boolean isServerSide() {
        return Utils.isServerSide();
    }

}
