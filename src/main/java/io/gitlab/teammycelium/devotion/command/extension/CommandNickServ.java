// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command.extension;

import io.gitlab.teammycelium.devotion.api.EiraIRCAPI;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.AuthManager;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.irc.IRCConnectionImpl;
import io.gitlab.teammycelium.devotion.util.ChatComponentBuilder;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

import java.util.List;

public class CommandNickServ implements SubCommand {

    @Override
    public String getCommandName() {
        return "nickserv";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.nickserv.usage";
    }

    @Override
    public String[] getAliases() {
        return new String[]{ "ns" };
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) throws CommandException {
        if (!serverSide) {
            ChatComponentBuilder.create().color('c').lang("devotion:general.serverOnlyCommand").send(sender);
            return true;
        }
        if (args.length < 2) {
            throw new WrongUsageException(getCommandUsage(sender));
        }
        IRCConnection connection;
        int argidx = 0;
        if (args.length >= 3) {
            IRCContext target = EiraIRCAPI.parseContext(null, args[0], IRCContext.ContextType.IRCConnection);
            if (target.getContextType() == IRCContext.ContextType.Error) {
                Utils.sendLocalizedMessage(sender, target.getName(), args[0]);
                return true;
            }
            connection = (IRCConnection) target;
            argidx = 1;
        } else {
            if (context == null) {
                Utils.sendLocalizedMessage(sender, "error.specifyServer");
                return true;
            }
            connection = context.getConnection();
        }
        ServerConfig serverConfig = ConfigurationHandler.getOrCreateServerConfig(connection.getHost());
        AuthManager.putNickServData(serverConfig.getIdentifier(), args[argidx], args[argidx + 1]);
        ConfigurationHandler.save();
        ((IRCConnectionImpl) connection).nickServIdentify();
        Utils.sendLocalizedMessage(sender, "commands.nickserv", connection.getHost());
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return false;
    }

}
