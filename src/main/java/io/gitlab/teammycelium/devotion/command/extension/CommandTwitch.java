// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command.extension;

import io.gitlab.teammycelium.devotion.ConnectionManager;
import io.gitlab.teammycelium.devotion.api.EiraIRCAPI;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.AuthManager;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.util.ChatComponentBuilder;
import io.gitlab.teammycelium.devotion.util.Globals;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

import java.util.List;

public class CommandTwitch implements SubCommand {

    @Override
    public String getCommandName() {
        return "twitch";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.twitch.usage";
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) throws CommandException {
        if (EiraIRCAPI.isConnectedTo(Globals.TWITCH_SERVER)) {
            ChatComponentBuilder.create().color('c').lang("devotion:error.alreadyConnected", "Twitch").send(sender);
            return true;
        }
        if (args.length == 0) {
            if (ConfigurationHandler.hasServerConfig(Globals.TWITCH_SERVER)) {
                Utils.sendLocalizedMessage(sender, "commands.connect", "Twitch");
                ServerConfig serverConfig = ConfigurationHandler.getOrCreateServerConfig(Globals.TWITCH_SERVER);
                ConnectionManager.connectTo(serverConfig);
                return true;
            } else {
                if (serverSide) {
                    throw new WrongUsageException(getCommandUsage(sender));
                } else {
                    ChatComponentBuilder.create().color('c').lang("devotion:general.serverOnlyCommand").send(sender);
                    return true;
                }
            }
        } else {
            if (args.length < 2) {
                throw new WrongUsageException("devotion:commands.twitch.usage");
            }
            ServerConfig serverConfig = ConfigurationHandler.getOrCreateServerConfig(Globals.TWITCH_SERVER);
            serverConfig.setNick(args[0]);
            AuthManager.putServerPassword(serverConfig.getIdentifier(), args[1]);
            serverConfig.getOrCreateChannelConfig("#" + serverConfig.getNick());
            serverConfig.getGeneralSettings().readOnly.set(false);
            serverConfig.getBotSettings().messageFormat.set("Twitch");
            ConfigurationHandler.addServerConfig(serverConfig);
            ConfigurationHandler.save();
            Utils.sendLocalizedMessage(sender, "commands.connect", "Twitch");
            ConnectionManager.connectTo(serverConfig);
            return true;
        }
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return true;
    }

}
