// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command.extension;

import io.gitlab.teammycelium.devotion.api.EiraIRCAPI;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.api.irc.IRCUser;
import io.gitlab.teammycelium.devotion.config.IgnoreList;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

import java.util.List;

public class CommandUnignore implements SubCommand {

    @Override
    public String getCommandName() {
        return "unignore";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.unignore.usage";
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) throws WrongUsageException {
        if (args.length < 1) {
            throw new WrongUsageException(getCommandUsage(sender));
        }
        IRCContext target = EiraIRCAPI.parseContext(context, args[0], IRCContext.ContextType.IRCUser);
        if (target.getContextType() == IRCContext.ContextType.Error) {
            Utils.sendLocalizedMessage(sender, target.getName(), args[0]);
            return true;
        }
        IRCUser user = (IRCUser) target;
        if (user.getHostname() == null) {
            Utils.sendLocalizedMessage(sender, "commands.unignore.notKnown", target.getName());
            return true;
        }
        if (!IgnoreList.isIgnored(user)) {
            Utils.sendLocalizedMessage(sender, "commands.unignore.notIgnored", target.getName());
            return true;
        }
        IgnoreList.removeFromIgnoreList(user);
        Utils.sendLocalizedMessage(sender, "commands.unignore.removed", target.getName());
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return true;
    }

}
