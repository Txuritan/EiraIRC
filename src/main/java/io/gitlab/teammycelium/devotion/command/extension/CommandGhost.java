// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command.extension;

import io.gitlab.teammycelium.devotion.api.EiraIRCAPI;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.AuthManager;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.config.base.ServiceConfig;
import io.gitlab.teammycelium.devotion.config.base.ServiceSettings;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.ICommandSender;

import java.util.List;

public class CommandGhost implements SubCommand {

    @Override
    public String getCommandName() {
        return "ghost";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.ghost.usage";
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) {
        IRCConnection connection;
        if (args.length > 0) {
            connection = EiraIRCAPI.parseContext(null, args[0], null).getConnection();
            if (connection == null) {
                Utils.sendLocalizedMessage(sender, "error.serverNotFound", args[0]);
                return true;
            }
        } else {
            if (context == null) {
                Utils.sendLocalizedMessage(sender, "error.specifyServer");
                return true;
            }
            connection = context.getConnection();
        }
        ServerConfig serverConfig = ConfigurationHandler.getOrCreateServerConfig(connection.getHost());
        ServiceSettings settings = ServiceConfig.getSettings(connection.getHost(), connection.getServerType());
        if (settings.hasGhostCommand()) {
            AuthManager.NickServData nickServData = AuthManager.getNickServData(serverConfig.getIdentifier());
            if (nickServData != null) {
                connection.irc(settings.getGhostCommand(nickServData.username, nickServData.password));
            }
        } else {
            Utils.sendLocalizedMessage(sender, "general.notSupported", "GHOST");
        }
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
        if (args.length == 0) {
            Utils.addConnectionsToList(list);
        }
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return true;
    }

}
