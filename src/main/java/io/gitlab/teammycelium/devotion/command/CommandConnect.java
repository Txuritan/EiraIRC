// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command;

import io.gitlab.teammycelium.devotion.ConnectionManager;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.AuthManager;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.util.ChatComponentBuilder;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

import java.util.List;

public class
CommandConnect implements SubCommand {

    @Override
    public String getCommandName() {
        return "connect";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.connect.usage";
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) throws CommandException {
        if (args.length < 1) {
            throw new WrongUsageException(getCommandUsage(sender));
        }
        String host = args[0];
        if (ConnectionManager.isConnectedTo(host)) {
            ChatComponentBuilder.create().color('c').lang("devotion:error.alreadyConnected", host).send(sender);
            return true;
        }
        Utils.sendLocalizedMessage(sender, "general.connecting", host);
        ServerConfig serverConfig = ConfigurationHandler.getOrCreateServerConfig(host);
        if (args.length >= 2) {
            AuthManager.putServerPassword(serverConfig.getIdentifier(), args[1]);
        }
        if (ConnectionManager.connectTo(serverConfig) != null) {
            ConfigurationHandler.addServerConfig(serverConfig);
            ConfigurationHandler.save();
        } else {
            Utils.sendLocalizedMessage(sender, "commands.connect.error", host);
        }
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
        if (args.length == 0) {
            for (ServerConfig serverConfig : ConfigurationHandler.getServerConfigs()) {
                list.add(serverConfig.getAddress());
            }
        }
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return true;
    }

}
