// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.command;

import io.gitlab.teammycelium.devotion.api.EiraIRCAPI;
import io.gitlab.teammycelium.devotion.api.SubCommand;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.AuthManager;
import io.gitlab.teammycelium.devotion.config.ChannelConfig;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.util.ConfigHelper;
import io.gitlab.teammycelium.devotion.util.IRCResolver;
import io.gitlab.teammycelium.devotion.util.Utils;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;

import java.util.List;

public class CommandJoin implements SubCommand {

    @Override
    public String getCommandName() {
        return "join";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "devotion:commands.join.usage";
    }

    @Override
    public String[] getAliases() {
        return null;
    }

    @Override
    public boolean processCommand(ICommandSender sender, IRCContext context, String[] args, boolean serverSide) throws CommandException {
        if (args.length < 1) {
            throw new WrongUsageException(getCommandUsage(sender));
        }
        IRCContext target = EiraIRCAPI.parseContext(null, args[0], IRCContext.ContextType.IRCConnection);
        IRCConnection connection;
        if (target.getContextType() == IRCContext.ContextType.Error) {
            if (args[0].indexOf('/') != -1) {
                Utils.sendLocalizedMessage(sender, target.getName(), args[0]);
                return true;
            } else {
                if (context == null) {
                    Utils.sendLocalizedMessage(sender, "error.specifyServer");
                    return true;
                }
                target = context.getConnection();
            }
        }
        connection = (IRCConnection) target;
        ServerConfig serverConfig = ConfigHelper.getServerConfig(connection);
        String channelName = IRCResolver.stripPath(args[0]);
        if (connection.getChannel(channelName) != null) {
            Utils.sendLocalizedMessage(sender, "error.alreadyJoined", channelName, connection.getHost());
            return true;
        }
        ChannelConfig channelConfig = serverConfig.getOrCreateChannelConfig(channelName);
        if (args.length >= 2) {
            AuthManager.putChannelPassword(channelConfig.getIdentifier(), args[1]);
        }
        Utils.sendLocalizedMessage(sender, "commands.join", channelConfig.getName(), connection.getHost());
        connection.join(channelConfig.getName(), AuthManager.getChannelPassword(channelConfig.getIdentifier()));
        return true;
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Utils.isOP(sender);
    }

    @Override
    public void addTabCompletionOptions(List<String> list, ICommandSender sender, String[] args) {
        if (args.length == 0) {
            for (ServerConfig serverConfig : ConfigurationHandler.getServerConfigs()) {
                for (ChannelConfig channelConfig : serverConfig.getChannelConfigs()) {
                    list.add(channelConfig.getName());
                }
            }
        }
    }

    @Override
    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    @Override
    public boolean hasQuickCommand() {
        return true;
    }

}
