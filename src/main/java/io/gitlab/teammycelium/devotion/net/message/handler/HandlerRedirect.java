package io.gitlab.teammycelium.devotion.net.message.handler;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.gitlab.teammycelium.devotion.DevotionIRC;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.net.message.MessageRedirect;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class HandlerRedirect implements IMessageHandler<MessageRedirect, IMessage> {
    @Override
    public IMessage onMessage(MessageRedirect message, MessageContext ctx) {
        DevotionIRC.proxy.addScheduledTask(() -> {
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(message.getRedirectConfig(), JsonObject.class);
            ServerConfig serverConfig = ServerConfig.loadFromJson(jsonObject);
            serverConfig.setIsRemote(true);

            DevotionIRC.proxy.handleRedirect(serverConfig);
        });
        return null;
    }
}
