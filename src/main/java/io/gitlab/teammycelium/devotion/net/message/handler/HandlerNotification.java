// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.net.message.handler;

import io.gitlab.teammycelium.devotion.DevotionIRC;
import io.gitlab.teammycelium.devotion.net.message.MessageNotification;
import io.gitlab.teammycelium.devotion.util.NotificationType;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class HandlerNotification implements IMessageHandler<MessageNotification, IMessage> {

    @Override
    public IMessage onMessage(MessageNotification message, MessageContext ctx) {
        DevotionIRC.proxy.addScheduledTask(() -> DevotionIRC.proxy.publishNotification(NotificationType.fromId(message.getNotificationType()), message.getText()));
        return null;
    }

}
