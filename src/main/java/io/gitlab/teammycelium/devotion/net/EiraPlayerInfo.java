// Copyright (c) 2015, Christopher "BlayTheNinth" Baker


package io.gitlab.teammycelium.devotion.net;

public class EiraPlayerInfo {

    private final String username;
    public String modVersion;
    public boolean modInstalled;

    public EiraPlayerInfo(String username) {
        this.username = username;
    }

    public boolean hasMod() {
        return modInstalled;
    }

    public String getUsername() {
        return username;
    }
}
