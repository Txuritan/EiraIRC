package io.gitlab.teammycelium.devotion.net;

import io.gitlab.teammycelium.devotion.net.message.MessageHello;
import io.gitlab.teammycelium.devotion.net.message.MessageNotification;
import io.gitlab.teammycelium.devotion.net.message.MessageRedirect;
import io.gitlab.teammycelium.devotion.net.message.handler.HandlerHello;
import io.gitlab.teammycelium.devotion.net.message.handler.HandlerNotification;
import io.gitlab.teammycelium.devotion.net.message.handler.HandlerRedirect;
import io.gitlab.teammycelium.devotion.util.Globals;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class NetworkHandler {

    public static final SimpleNetworkWrapper instance = NetworkRegistry.INSTANCE.newSimpleChannel(Globals.MOD_ID);

    public static void init() {
        instance.registerMessage(HandlerHello.class, MessageHello.class, 0, Side.SERVER);
        instance.registerMessage(HandlerNotification.class, MessageNotification.class, 1, Side.CLIENT);
        instance.registerMessage(HandlerRedirect.class, MessageRedirect.class, 4, Side.CLIENT);
    }
}
