// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.net.message.handler;

import io.gitlab.teammycelium.devotion.DevotionIRC;
import io.gitlab.teammycelium.devotion.net.EiraPlayerInfo;
import io.gitlab.teammycelium.devotion.net.message.MessageHello;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class HandlerHello implements IMessageHandler<MessageHello, IMessage> {

    @Override
    public IMessage onMessage(MessageHello packet, MessageContext ctx) {
        DevotionIRC.proxy.addScheduledTask(() -> {
            EntityPlayer entityPlayer = ctx.getServerHandler().player;

            EiraPlayerInfo playerInfo = DevotionIRC.instance.getNetHandler().getPlayerInfo(entityPlayer.getName());
            playerInfo.modInstalled = true;
            playerInfo.modVersion = packet.getVersion();
        });
        return null;
    }

}
