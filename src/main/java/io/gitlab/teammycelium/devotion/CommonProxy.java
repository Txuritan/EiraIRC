// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion;

import io.gitlab.teammycelium.devotion.api.config.IConfigManager;
import io.gitlab.teammycelium.devotion.api.event.IRCChannelMessageEvent;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.config.LocalConfig;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.config.SharedGlobalConfig;
import io.gitlab.teammycelium.devotion.net.NetworkHandler;
import io.gitlab.teammycelium.devotion.net.message.MessageNotification;
import io.gitlab.teammycelium.devotion.util.NotificationType;
import net.minecraft.command.ICommandSender;
import net.minecraft.crash.CrashReport;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommonProxy {

    public void init() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void postInit() {
    }

    public void publishNotification(NotificationType type, String text) {
        NetworkHandler.instance.sendToAll(new MessageNotification(type, text));
    }

    public String getUsername() {
        return null;
    }

    public void loadConfig(File configDir, boolean reloadFile) {
        SharedGlobalConfig.load(configDir, reloadFile);
        LocalConfig.load(configDir, reloadFile);
    }

    public void handleRedirect(ServerConfig serverConfig) {
    }

    public boolean handleConfigCommand(ICommandSender sender, String key, String value) {
        return SharedGlobalConfig.handleConfigCommand(sender, key, value);
    }

    public String handleConfigCommand(ICommandSender sender, String key) {
        return SharedGlobalConfig.handleConfigCommand(sender, key);
    }

    public void addConfigOptionsToList(List<String> list, String option, boolean autoCompleteOption) {
        SharedGlobalConfig.addOptionsToList(list, option, autoCompleteOption);
    }

    public boolean checkClientBridge(IRCChannelMessageEvent event) {
        return false;
    }

    public void saveConfig() {
        if (SharedGlobalConfig.thisConfig.hasChanged()) {
            SharedGlobalConfig.thisConfig.save();
        }
        if (LocalConfig.thisConfig.hasChanged()) {
            LocalConfig.thisConfig.save();
        }
    }

    public void handleException(IRCConnection connection, Exception e) {
        DevotionIRC.logger.error("Encountered an unexpected exception", e);
        CrashReport report = FMLCommonHandler.instance().getMinecraftServerInstance().addServerInfoToCrashReport(new CrashReport("Exception in IRC Connection " + connection.getHost(), e));
        File file1 = new File(new File(FMLCommonHandler.instance().getMinecraftServerInstance().getDataDirectory(), "crash-reports"), "crash-" + (new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss")).format(new Date()) + "-server.txt");
        if (report.saveToFile(file1)) {
            DevotionIRC.logger.error("This crash report has been saved to: " + file1.getAbsolutePath());
        } else {
            DevotionIRC.logger.error("We were unable to save this crash report to disk.");
        }
        FMLCommonHandler.instance().getMinecraftServerInstance().stopServer();
    }

    @SubscribeEvent
    public void onTick(TickEvent.ServerTickEvent event) {
        ConnectionManager.tickConnections();
    }

    public IConfigManager getClientGlobalConfig() {
        return null;
    }

    public void addScheduledTask(Runnable runnable) {
        FMLCommonHandler.instance().getMinecraftServerInstance().addScheduledTask(runnable);
    }
}
