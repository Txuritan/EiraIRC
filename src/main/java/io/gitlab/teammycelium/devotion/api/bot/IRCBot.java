// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.bot;

import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;

public interface IRCBot {

    /**
     * @return the irc connection this bot is running on
     */
    IRCConnection getConnection();

    /**
     * @return true if this is a server-side bot
     */
    boolean isServerSide();

    /**
     * Should be run from within a {@code ReloadBotCommandsEvent}
     *
     * @param command the command implementation to be registered
     */
    void registerCommand(IBotCommand command);

}
