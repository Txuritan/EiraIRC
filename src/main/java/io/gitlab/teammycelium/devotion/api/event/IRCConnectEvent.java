// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.event;

import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCMessage;

/**
 * This event is published on the MinecraftForge.EVENTBUS bus whenever EiraIRC successfully connects to an IRC server.
 */
public class IRCConnectEvent extends IRCRawMessageEvent {

    /**
     * INTERNAL EVENT. YOU SHOULD NOT POST THIS YOURSELF.
     *
     * @param connection the connection that was created
     */
    public IRCConnectEvent(IRCConnection connection, IRCMessage rawMessage) {
        super(connection, rawMessage);
    }

}
