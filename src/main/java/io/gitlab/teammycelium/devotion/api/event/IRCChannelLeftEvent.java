// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.event;

import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;

/**
 * This event is published on the MinecraftForge.EVENTBUS bus whenever EiraIRC leaves a channel.
 */
public class IRCChannelLeftEvent extends IRCEvent {

    /**
     * the channel that was left
     */
    public final IRCChannel channel;

    /**
     * INTERNAL EVENT. YOU SHOULD NOT POST THIS YOURSELF.
     *
     * @param connection the connection the channel that was left is on
     * @param channel    the channel that was left
     */
    public IRCChannelLeftEvent(IRCConnection connection, IRCChannel channel) {
        super(connection);
        this.channel = channel;
    }

}
