package io.gitlab.teammycelium.devotion.api.irc;

@SuppressWarnings("unused")
public interface IRCMessage {
    String getTagByKey(String key);

    String getPrefix();

    String getPrefixNick();

    String getPrefixUsername();

    String getPrefixHostname();

    String getCommand();

    String arg(int i);

    int argCount();
}
