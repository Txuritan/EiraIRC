// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.event;

import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCMessage;

/**
 * Base class for events based on a raw IRC message.
 */
public abstract class IRCRawMessageEvent extends IRCEvent {

    /**
     * the raw message
     */
    public final IRCMessage rawMessage;

    /**
     * INTERNAL EVENT. YOU SHOULD NOT POST THIS YOURSELF.
     *
     * @param connection the connection this event is based on
     */
    public IRCRawMessageEvent(IRCConnection connection, IRCMessage rawMessage) {
        super(connection);
        this.rawMessage = rawMessage;
    }

}
