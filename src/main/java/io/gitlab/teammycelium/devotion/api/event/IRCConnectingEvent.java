// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.event;

import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;

/**
 * This event is published on the MinecraftForge.EVENTBUS bus whenever EiraIRC attempts to connect to an IRC server.
 */
public class IRCConnectingEvent extends IRCEvent {

    /**
     * INTERNAL EVENT. YOU SHOULD NOT POST THIS YOURSELF.
     *
     * @param connection the connection that is being created
     */
    public IRCConnectingEvent(IRCConnection connection) {
        super(connection);
    }

}
