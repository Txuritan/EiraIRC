// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.api.event;

import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCMessage;

/**
 * This event is published on the MinecraftForge.EVENTBUS bus whenever EiraIRC successfully joined a channel.
 */
public class IRCChannelJoinedEvent extends IRCRawMessageEvent {

    /**
     * the channel that was joined
     */
    public final IRCChannel channel;

    /**
     * INTERNAL EVENT. YOU SHOULD NOT POST THIS YOURSELF.
     *
     * @param connection the connection the channel that was joined is on
     * @param channel    the channel that was joined
     */
    public IRCChannelJoinedEvent(IRCConnection connection, IRCMessage rawMessage, IRCChannel channel) {
        super(connection, rawMessage);
        this.channel = channel;
    }

}
