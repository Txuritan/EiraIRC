package io.gitlab.teammycelium.devotion.addon;

import io.gitlab.teammycelium.devotion.DevotionIRC;
import io.gitlab.teammycelium.devotion.api.event.InitConfigEvent;
import io.gitlab.teammycelium.devotion.config.ChannelConfig;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.config.SharedGlobalConfig;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;

public class Compatibility {

    private static boolean isEiraMoticonsInstalled;
    private static boolean isTabbyChat2Installed;

    public static boolean isEiraMoticonsInstalled() {
        return isEiraMoticonsInstalled;
    }

    public static boolean isTabbyChat2Installed() {
        return isTabbyChat2Installed;
    }

    public static void postInit(FMLPostInitializationEvent event) {
        // event.buildSoftDependProxy("Dynmap", "io.gitlab.teammycelium.devotion.addon.DynmapWebChatAddon");

        if (event.getSide() == Side.CLIENT) {
            // event.buildSoftDependProxy("TabbyChat2", "io.gitlab.teammycelium.devotion.addon.TabbyChat2Addon");
            // event.buildSoftDependProxy("eiramoticons", "io.gitlab.teammycelium.devotion.addon.EiraMoticonsAddon");
            new FancyOverlay();
        }

        MinecraftForge.EVENT_BUS.post(new InitConfigEvent.SharedGlobalSettings(SharedGlobalConfig.manager));
        if (DevotionIRC.proxy.getClientGlobalConfig() != null) {
            MinecraftForge.EVENT_BUS.post(new InitConfigEvent.ClientGlobalSettings(DevotionIRC.proxy.getClientGlobalConfig()));
        }
        MinecraftForge.EVENT_BUS.post(new InitConfigEvent.GeneralSettings(SharedGlobalConfig.generalSettings.manager));
        MinecraftForge.EVENT_BUS.post(new InitConfigEvent.BotSettings(SharedGlobalConfig.botSettings.manager));
        MinecraftForge.EVENT_BUS.post(new InitConfigEvent.ThemeSettings(SharedGlobalConfig.theme.manager));

        for (ServerConfig serverConfig : ConfigurationHandler.getServerConfigs()) {
            MinecraftForge.EVENT_BUS.post(new InitConfigEvent.GeneralSettings(serverConfig.getGeneralSettings().manager));
            MinecraftForge.EVENT_BUS.post(new InitConfigEvent.BotSettings(serverConfig.getBotSettings().manager));
            MinecraftForge.EVENT_BUS.post(new InitConfigEvent.ThemeSettings(serverConfig.getTheme().manager));
            for (ChannelConfig channelConfig : serverConfig.getChannelConfigs()) {
                MinecraftForge.EVENT_BUS.post(new InitConfigEvent.GeneralSettings(channelConfig.getGeneralSettings().manager));
                MinecraftForge.EVENT_BUS.post(new InitConfigEvent.BotSettings(channelConfig.getBotSettings().manager));
                MinecraftForge.EVENT_BUS.post(new InitConfigEvent.ThemeSettings(channelConfig.getTheme().manager));
            }
        }

        // isEiraMoticonsInstalled = Loader.isModLoaded("eiramoticons");
        // isTabbyChat2Installed = Loader.isModLoaded("TabbyChat2");
    }
}
