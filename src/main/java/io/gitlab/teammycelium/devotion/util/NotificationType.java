// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.util;

public enum NotificationType {
    FriendJoined,
    PlayerMentioned,
    PrivateMessage;

    private static final NotificationType[] values = values();

    public static NotificationType fromId(int id) {
        return values[id];
    }

}
