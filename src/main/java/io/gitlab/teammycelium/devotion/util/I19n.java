// Copyright (c) 2015, Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.util;

import net.minecraft.util.text.translation.I18n;

public class I19n {

    public static String format(String key, Object... params) {
        return I18n.translateToLocalFormatted(key, params);
    }

}
