// Copyright (c) 2015 Christopher "BlayTheNinth" Baker

package io.gitlab.teammycelium.devotion.util;

import io.gitlab.teammycelium.devotion.api.irc.IRCChannel;
import io.gitlab.teammycelium.devotion.api.irc.IRCConnection;
import io.gitlab.teammycelium.devotion.api.irc.IRCContext;
import io.gitlab.teammycelium.devotion.config.ChannelConfig;
import io.gitlab.teammycelium.devotion.config.ConfigurationHandler;
import io.gitlab.teammycelium.devotion.config.ServerConfig;
import io.gitlab.teammycelium.devotion.config.SharedGlobalConfig;
import io.gitlab.teammycelium.devotion.config.settings.BotSettings;
import io.gitlab.teammycelium.devotion.config.settings.GeneralSettings;
import io.gitlab.teammycelium.devotion.config.settings.ThemeSettings;
import io.gitlab.teammycelium.devotion.irc.IRCConnectionImpl;

public class ConfigHelper {

    public static String formatNick(String format) {
        String s = format;
        s = s.replace("%USERNAME%", Utils.getUsername());
        s = s.replace("%ANONYMOUS%", "justinfan" + String.valueOf((int) (Math.random() * 1000000)) + String.valueOf(((int) (Math.random() * 1000000))));
        return s;
    }

    public static String getNick(ServerConfig serverConfig) {
        if (serverConfig.getNick() != null && !serverConfig.getNick().isEmpty()) {
            return serverConfig.getNick();
        }
        return Globals.DEFAULT_NICK;
    }

    public static String getQuitMessage(IRCConnection connection) {
        return ConfigurationHandler.getOrCreateServerConfig(connection.getHost()).getBotSettings().quitMessage.get();
    }

    public static ServerConfig getServerConfig(IRCConnection connection) {
        return ((IRCConnectionImpl) connection).getServerConfig();
    }

    public static ChannelConfig getChannelConfig(IRCChannel channel) {
        return getServerConfig(channel.getConnection()).getOrCreateChannelConfig(channel);
    }

    public static ThemeSettings getTheme(IRCContext context) {
        if (context instanceof IRCChannel) {
            return getChannelConfig((IRCChannel) context).getTheme();
        }
        return SharedGlobalConfig.theme;
    }

    public static BotSettings getBotSettings(IRCContext context) {
        if (context instanceof IRCChannel) {
            return getChannelConfig((IRCChannel) context).getBotSettings();
        }
        return SharedGlobalConfig.botSettings;
    }

    public static GeneralSettings getGeneralSettings(IRCContext context) {
        if (context instanceof IRCChannel) {
            return getChannelConfig((IRCChannel) context).getGeneralSettings();
        }
        return SharedGlobalConfig.generalSettings;
    }

    public static ServerConfig resolveServerConfig(String target) {
        int pathSplitIndex = target.indexOf('/');
        if (pathSplitIndex != -1) {
            target = target.substring(0, pathSplitIndex - 1);
        }
        return ConfigurationHandler.getServerConfig(target);
    }

    public static ChannelConfig resolveChannelConfig(String target) {
        int pathSplitIndex = target.indexOf('/');
        ServerConfig serverConfig = null;
        if (pathSplitIndex != -1) {
            serverConfig = ConfigurationHandler.getServerConfig(target.substring(0, pathSplitIndex - 1));
            target = target.substring(pathSplitIndex + 1);
        }
        if (serverConfig != null) {
            return serverConfig.getChannelConfig(target);
        } else {
            for (ServerConfig config : ConfigurationHandler.getServerConfigs()) {
                if (config.hasChannelConfig(target)) {
                    return config.getChannelConfig(target);
                }
            }
        }
        return null;
    }
}
